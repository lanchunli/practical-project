package com.li.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.li.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 数据层  对数据库进行操作
 */
@Repository
@Mapper
public  interface Productdao extends BaseMapper<Product> {

}
