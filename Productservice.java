package com.li.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.li.pojo.Product;

/**
 * 业务层接口  继承了mybatisplus的Iservice接口
 */
public interface Productservice extends IService<Product> {

}
