package com.li.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.li.dao.Productdao;
import com.li.pojo.Product;
import org.springframework.stereotype.Service;

/**
 * 实现了业务层的接口
 */
@Service
public class Productserviceimpl extends ServiceImpl<Productdao, Product>  implements Productservice {

}
