/*
 Navicat MySQL Data Transfer

 Source Server         : Li
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : product

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 01/05/2022 14:48:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `spec` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `price` int(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (26, '火锅三秒铃铃卷豆腐卷（进口）120g', '120g/包', 22);
INSERT INTO `product` VALUES (27, '洪小拳卤鸡爪原味150g', '150g/盒', 13);
INSERT INTO `product` VALUES (28, '洪小拳香酥花生100g', '100g/盒', 5);
INSERT INTO `product` VALUES (30, '蓉妈妈米糕（芝麻）260g/盒', '260g/盒', 6);
INSERT INTO `product` VALUES (31, '祥广记猪肉菌菇鲜饺240g', '240g/份', 10);
INSERT INTO `product` VALUES (32, '品胜葱油拌面（两份装）250g', '250g/袋（含料包）', 4);
INSERT INTO `product` VALUES (33, '素天下日式福棉冻豆腐350g', '350g/包', 9);
INSERT INTO `product` VALUES (34, '圃美多汤用豆腐400g', '400g/份', 6);
INSERT INTO `product` VALUES (35, '必品阁韩式炸鸡 500g/盒', '500g/盒', 38);
INSERT INTO `product` VALUES (36, '周黑鸭卤鸭锁骨【趣享】锁鲜120g', '120g', 8);
INSERT INTO `product` VALUES (37, '如意三宝五香卤牛肉200g', '200g/包', 36);
INSERT INTO `product` VALUES (38, '凤祥优形沙拉鸡胸肉（新奥尔良味）100g', '100g/袋', 13);
INSERT INTO `product` VALUES (39, '精丰粗粮南瓜馒头（6个装）180g', '180g/盒', 9);
INSERT INTO `product` VALUES (40, '每食造物卤牛肉酱香味105g', '105g/盒', 14);
INSERT INTO `product` VALUES (41, '每食造物无骨凤爪百香果味180g', '180克/盒', 14);
INSERT INTO `product` VALUES (42, '如意三宝蜜汁烤五花216g', '216g/份', 16);
INSERT INTO `product` VALUES (43, '爪不同小龙虾（十三香味）250g', '250g/盒', 25);
INSERT INTO `product` VALUES (44, '洪湖渔家麻辣小龙虾800g', '800g/份', 33);
INSERT INTO `product` VALUES (45, '周黑鸭卤鸭脖【趣享】锁鲜120g', '120g/份', 13);
INSERT INTO `product` VALUES (46, '火锅三秒铃铃卷豆腐卷（进口）120g', '120g/包', 13);
INSERT INTO `product` VALUES (47, '洪小拳卤鸡爪原味150g', '150g/盒', 13);
INSERT INTO `product` VALUES (48, '洪小拳香酥花生100g', '100g/盒', 5);
INSERT INTO `product` VALUES (49, '周黑鸭川香藤椒鸭掌【趣享】110g', '110g/盒', 13);
INSERT INTO `product` VALUES (50, '蓉妈妈米糕（芝麻）260g/盒', '260g/盒', 6);
INSERT INTO `product` VALUES (51, '祥广记猪肉菌菇鲜饺240g', '240g/份', 10);
INSERT INTO `product` VALUES (52, '品胜葱油拌面（两份装）250g', '250g/袋（含料包）', 4);
INSERT INTO `product` VALUES (53, '素天下日式福棉冻豆腐350g', '350g/包', 9);
INSERT INTO `product` VALUES (54, '圃美多汤用豆腐400g', '400g/份', 6);
INSERT INTO `product` VALUES (55, '必品阁韩式炸鸡 500g/盒', '500g/盒', 38);
INSERT INTO `product` VALUES (56, '周黑鸭卤鸭锁骨【趣享】锁鲜120g', '120g', 8);
INSERT INTO `product` VALUES (57, '如意三宝五香卤牛肉200g', '200g/包', 36);
INSERT INTO `product` VALUES (58, '凤祥优形沙拉鸡胸肉（新奥尔良味）100g', '100g/袋', 13);
INSERT INTO `product` VALUES (59, '精丰粗粮南瓜馒头（6个装）180g', '180g/盒', 9);

SET FOREIGN_KEY_CHECKS = 1;
