package com.li.pojo;

import lombok.Data;

/**
 * 实体类
 */
@Data
public class Product {
    public int id;
    public String productname;
    public String spec;
    public int price;
}
