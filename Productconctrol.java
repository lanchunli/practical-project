package com.li.conctrol;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.li.pojo.Product;
import com.li.service.Productservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

/**
 * 控制层  对页面实现操作
 */
@RestController
@RequestMapping("/produc")
public class Productconctrol {
    @Autowired
    private Productservice userService;

    @PostMapping("/urlpath")
    public void saves(@RequestBody String path) throws IOException {
        String paths = (String) JSON.parseObject(path).get("paths");
        String s = "";
        URL url = new URL(paths);
        //用字符处理流获取url的数据
        BufferedReader reaDer = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder builder = new StringBuilder();
        String str = "";
        //使数据添加入StringBuilder里
        while ((str = reaDer.readLine()) != null) {
            builder.append(str);
        }
        String string = builder.toString();
        //解析成JSONObject
        JSONObject object = (JSONObject) JSONObject.parse(string);
        JSONArray jsonObject = object.getJSONObject("data").getJSONArray("products");
        for (Object o : jsonObject) {
            Product product = new Product();
            product.productname = (String) JSON.parseObject(JSON.toJSONString(o)).get("name");
            product.spec = (String) JSON.parseObject(JSON.toJSONString(o)).get("spec");
            product.price = ((int) JSON.parseObject(JSON.toJSONString(o)).get("price")) / 100;
            QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("productname",product.productname)
                    .eq("spec",product.spec)
                    .eq("price",product.price);
            Product one = userService.getOne(queryWrapper);
            if (one==null){
                userService.save(product);
            }
        }
    }

    @PostMapping("/insert")
    public boolean insert(@RequestBody String form) {
        Product newproduct = new Product();
        newproduct.productname = (String) JSON.parseObject(form).getJSONObject("form").get("productname");
        newproduct.price = Integer.parseInt((String) JSON.parseObject(form).getJSONObject("form").get("price"));
        newproduct.spec = (String) JSON.parseObject(form).getJSONObject("form").get("spec");
        return userService.save(newproduct);
    }

    @DeleteMapping("/{id}")
    public boolean remove(@PathVariable int id) {
        return userService.removeById(id);
    }

    @GetMapping("/all")
    public List<Product> getall(@RequestParam("name") String name) {
        String trim = name.trim();
        if (trim.isEmpty()) {
            return userService.list();
        } else {
            QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
            productQueryWrapper.like("productname", trim);
            return userService.list(productQueryWrapper);
        }
    }
}



